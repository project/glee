# Macros

This is a storage directory for Twig macro functions. For more information about macros, refer to the [Twig documentation](https://twig.symfony.com/doc/3.x/tags/macro.html).
