# Glee Starter Kit - Sass + KSS Style Guide

## Introduction

Glee is a bare minimum Drupal theme to help you create a beautiful, highly-custom UI that will make everyone go "glee"! Similar to Stark, Glee utilizes Drupal's default HTML and CSS styles to get you started in a pinch without the bloat and pain of overriding unnecessary styles.

## Installation

1. Copy this directory to /themes/custom and replace the theme name in front of `.info.yml`, `.libraries.yml`, and `.theme` files with your own.
2. Navigate to /admin/appearance.
3. Install and set your new custom theme as the default theme.
4. Run `gulp init` to get started!

## Theme Structure

```
components
├── 00-macros/
├── 01-core/
│   ├── mixins/
│   ├── _functions.scss
│   ├── _overrides.scss
│   ├── _variables.scss
├── 02-component/
├── 03-layout/
├── styles.scss
.
..
```

- `00-macro`: A storage directory for Twig macro functions.
- `01-core`: A storage directory for `_functions.scss`, `_mixins.scss`, and `_variables.scss`.
- `02-element`: The building blocks that are used to create larger parts of the UI such as form fields, links, and buttons.
- `03-component`: A simple groups of elements that combine to form a reusable unit.
- `04-layout`: The page templates that visitors see on the site., i.e., component layouts on templates, Drupal regions, etc.
- `style.scss`: The primary file to import partial Sass files, including theme variables, site utilities, and component styling.

## Usage

Out of the box, Glee contains many helpful npm scripts:

```
npm run [command]
```

- `init`: Installs the npm dependencies and runs the default Gulp task.
- `build`: Runs the default Gulp task; compiles Sass into CSS, generates the style guide, and minifies the image assets in the /images directory.
- `lint`: Lints Sass and JavaScript assets in the theme directory.
- `styleguide`: Generates the KSS style guide.
- `imagemin`: Minifies the image assets in the /images directory.
- `watch`: "Watches" for changes to the _.scss and _.js files and runs similiar tasks to the build task.
