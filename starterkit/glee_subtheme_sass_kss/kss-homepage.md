# Glee

## Branding and Component Library

### Overview

This branding and component library is a developer's guideline to the user interface, helping us to look at the design both as a cohesive whole and collection of building blocks. As this library grows, feel free to add more information to help your team identity the components used on the site and maintain best front-end development practices.

### Resources

- [Glee](https://www.drupal.org/project/glee)
- [Building Style Guides with `kss-node`](https://kss-node.github.io/kss-node/)
