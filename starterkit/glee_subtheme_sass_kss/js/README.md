# JavaScript

A storage directory for custom JavaScript files. The JavaScript gulp task is currently set-up to lint any changes made in this folder. To include any additional JavaScript assets, see `paths.js` in `package.json`.
