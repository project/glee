INTRODUCTION
------------

Glee is a bare minimum Drupal theme to help you create a beautiful, 
highly-custom UI that will make everyone go "glee"! Similar to Stark, 
Glee utilizes Drupal's default HTML and CSS styles to get you started in 
a pinch without the bloat and pain of overriding unnecessary styles. 


INSTALLATION
------------

After downloading the theme: 

1. Copy one of the starter kits to /themes/custom and replace the 
theme name in the appropriate files and code with your own. 
2. Navigate to /admin/appearance.
3. Install and set your new custom theme as the default theme.
4. Run [gulp init] to get started!


STARTER KITS
------------

Glee ships with three types of starter kits to help you get started.
If you like developing your theme SASS-y and spicy, use one of the
Sass (Syntactically Awesome Style Sheets) sub-themes - 
[glee_subtheme_sass_kss] or [glee_subtheme_sass_fractal] (coming soon). 
Otherwise, use [glee_subtheme_css] (coming soon) if you like to keep it 
plain and simple.

For additional information about each starter kit, please refer to the
README file within its directory.